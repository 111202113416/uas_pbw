const container = document.querySelector('.container');
const seats = document.querySelectorAll('.row .seat:not(.occupied');
const count = document.getElementById('count');
const total = document.getElementById('total');

let ticketPrice =40000+1000;

// update total and count
function updateSelectedCount() {
  const selectedSeats = document.querySelectorAll('.row .seat.selected');

  const seatsIndex = [...selectedSeats].map((seat) => [...seats].indexOf(seat));

  localStorage.setItem('selectedSeats', JSON.stringify(seatsIndex));

  //copy selected seats into arr
  // map through array
  //return new array of indexes

  const selectedSeatsCount = selectedSeats.length;

  count.innerText = selectedSeatsCount;
  total.innerText = selectedSeatsCount * ticketPrice;
}

// populateUI();
// function populateUI() {
//   const selectedSeats = JSON.parse(localStorage.getItem('selectedSeats'));
//   if (selectedSeats !== null && selectedSeats.length > 0) {
//     seats.forEach((seat, index) => {
//       if (selectedSeats.indexOf(index) > -1) {
//         seat.classList.add('selected');
//       }
//     });
//   }}

container.addEventListener('click', (e) => {
  if (e.target.classList.contains('seat') && !e.target.classList.contains('occupied')) {
    e.target.classList.toggle('selected');
    updateSelectedCount();
  }
});

var div =document.querySelectorAll('.row .seat.occupied');
for (var i = 0; i < div.length; i++) {
div[i].addEventListener('click', function(){
  alert('Seat occcupied! Please choose another seat.');
});
} 

updateSelectedCount();

function book() {

var div = document.querySelectorAll('.row .seat.selected');
for (var i = 0; i < div.length; i++) {
  div[i].classList.remove('selected');
  div[i].classList.add('occupied')
}
  alert('Successfully booked!');
  updateSelectedCount();
}
